from home.models import Product


CART_SESSION='cart'

class Cart:
    def __init__(self , request):
        self.session = request.session
        cart=self.session.get(CART_SESSION)
        if not cart:
            cart=self.session[CART_SESSION]={}
        self.cart=cart


    def __iter__(self):
        products_ids=self.cart.keys()
        products=Product.objects.filter(id__in=products_ids)
        cart=self.cart.copy()
        for product in products:
            cart[str(product.id)]['product'] = product
        
        for item in cart.values():
            item['total_price'] = int(item['price'])* item['quantity']
            yield item

    

    def add(self, product , quantity):
        product_id=str(product.id)
        if product_id not in self.cart:
            self.cart[product_id]={'quantity':0 
                                ,'price':str(product.price)} 
            self.cart[product_id]['quantity'] += quantity
            self.save()
        

    def __len__(self):
        return sum(item['quantity'] for item in self.cart.values())
    
    

    def remove(self , product):
        product_id =str(product.id) 
        if product_id in self.cart:
            del self. cart[product_id]
            self.save()


    def save(self):
        self.session.modified = True


    def get_total_price(self):
        sum_carts = 0

        if not self.cart:
            return sum_carts  # return 0 if the cart is empty
    
        for item in self.cart.values():
            sum_cart = int(item['price']) * item['quantity']
            sum_carts += sum_cart
        return sum_carts
    


    def clear(self):
        del self.session[CART_SESSION]
        self.save()
        
   

