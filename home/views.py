from django.shortcuts import render , get_object_or_404
from django.views import View 
from . models import *
from orders.forms import CartAddForm



class HomeView(View):
    def get(self, request, category_slug=None):
        products = Product.objects.filter(available=True)
        categories = Category.objects.filter(is_sub=False)
        subcategories = Category.objects.filter(is_sub=True)
        
        if category_slug:
            category = get_object_or_404(Category, slug=category_slug)
            products = products.filter(category=category)
            subcategories = category.scategory.all()

        return render(request, 'home/home.html', {'products': products, 'categories': categories, 'subcategories': subcategories})


class ProductDetailView(View):
    def get(self , request , slug):
        form=CartAddForm()
        product = get_object_or_404(Product , slug=slug)
        return render(request , 'home/detail.html', {'product':product , 'form':form })