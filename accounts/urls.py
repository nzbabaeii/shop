from django.urls import path
from . import views

app_name='accounts'
urlpatterns =[
    path('register/',views.UserRegisterView.as_view() , name='Register'),
    path('verify/',views.UserVerifyView.as_view(), name='Verify'),
    path('login/' , views.LoginView.as_view(), name='Login'),
    path('logout/' , views.LogoutView.as_view(), name='Logout'),

]