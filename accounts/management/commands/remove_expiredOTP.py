from typing import Any
from django.core.management.base import BaseCommand
from accounts.models import OTP
from datetime import datetime,timedelta

class Commands(BaseCommand):
    help='remove expired otps'

    def handle(self, *args, **options):
        expire=datetime.now() - timedelta(minutes=2)
        OTP.objects.filter(created__lt=expire).delete()
        self.stdout.write('all expired opts are removed')