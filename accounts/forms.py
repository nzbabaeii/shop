from django import forms
from .models import User ,OTP
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.validators import RegexValidator

class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'class':'form-control'}))
    password2 = forms.CharField(
    label="Password confirmation", widget=forms.PasswordInput(attrs={'class':'form-control'}))

    class Meta:
        model = User
        fields ='__all__'

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError("Passwords don't match")
        return password2
    


    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user



class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(help_text='u cant change pass using <a href=\"../password/\" this form </a>.')

    class Meta:
        model =User
        fields = "__all__"



class UserRegisterForm(forms.Form):
    email=forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control'}))
    phone_regex = RegexValidator(regex=r'^(\+\d{1,3})?,?\s?\d{8,13}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone=forms.CharField(validators=[phone_regex],max_length=11,widget=forms.TextInput(attrs={'class':'form-control'}))
    full_name=forms.CharField(label='first name & last name',widget=forms.TextInput(attrs={'class':'form-control'}))
    password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))
    password2=forms.CharField(label='confirm password',widget=forms.PasswordInput(attrs={'class':'form-control'}))


    def clean_email(self):
        email=self.cleaned_data['email']
        user =User.objects.filter(email=email).exists()
        if user :
            raise ValidationError('this email already exist ')
        return email
    
    def clean_phone(self):
        phone=self.cleaned_data['phone']
        user =User.objects.filter(phone_number=phone).exists()
        if user :
            raise ValidationError('this phone number already exist ')
        OTP.objects.filter(phone_number=phone).delete()
        return phone
    
    def clean(self):
        data=super().clean()
        p1=data.get('password')
        p2= data.get('password2')
      
        if p1 and p2 and p1 != p2:
            raise ValidationError('passwords must match')

    

class VerifycodeForm(forms.Form):
    code = forms.IntegerField()
    


class LoginForm(forms.Form):
     phone_number = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
     password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))


