from celery import shared_task
from accounts.models import OTP
from datetime import datetime,timedelta
import pytz


@shared_task
def remove_expired_otps():
    expire=datetime.now(tz=pytz.timezone('Asia/Tehran')) - timedelta(minutes=2)
    OTP.objects.filter(created__lt=expire).delete()
    