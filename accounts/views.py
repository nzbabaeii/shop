from django.shortcuts import render , redirect
from django.contrib import messages
from django.views import View
from .forms import UserRegisterForm , VerifycodeForm ,LoginForm
import random
from django.contrib.auth import authenticate , login , logout
from django.contrib.auth.mixins import LoginRequiredMixin
from utils import send_OTP
from .models import OTP , User

class UserRegisterView(View):
    form_class = UserRegisterForm

    def dispatch(self, request , *args, **kwargs) :
        if request.user.is_authenticated:
            return redirect('home:home')
        return super().dispatch(request,*args ,**kwargs)

    def get(self, request):
        form = self.form_class
        return render(request , 'accounts/register.html',{'form':form})
    
    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            random_code = random.randint(1000,9999)
            send_OTP(form.cleaned_data['phone'],random_code)
            OTP.objects.create(phone_number=form.cleaned_data['phone'],code=random_code)
            request.session['info_register']={
                'phone_number': form.cleaned_data['phone'],
                'email':form.cleaned_data['email'],
                'full_name':form.cleaned_data['full_name'],
                'password': form.cleaned_data['password'],
            }
            messages.success(request,'code was sent','success')
            return redirect('accounts:Verify')
        
        return render(request,'accounts/register.html',{'form':form})


class UserVerifyView(View):
    form_class = VerifycodeForm

    def dispatch(self, request , *args, **kwargs) :
        if request.user.is_authenticated:
            return redirect('home:home')
        return super().dispatch(request,*args ,**kwargs)

    def get(self,request):
        form=self.form_class
        return render(request ,'accounts/verify.html',{'form':form})
    
    def post(self,request):
        user_session = request.session['info_register']
        code_instance = OTP.objects.get(phone_number=user_session['phone_number'])
        form = self.form_class(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            if cd['code'] == code_instance.code:
                User.objects.create_user(user_session['phone_number'] ,user_session['email'] ,
                                          user_session['full_name'],user_session['password'])
                
                code_instance.delete()
                messages.success(request,'register success','success')
                return redirect('home:home')
            
            else:
                messages.error(request, 'wrong code','danger')
                return redirect('accounts:Verify')
        return render(request , 'accounts/register.html' , {'form':form})
    



class LoginView(View):
    form_class =LoginForm
    template_name = 'accounts/login.html'

    def setup(self , request , *args , **kwargs):
        self.next=request.GET.get('next')
        return super().setup(request, *args, **kwargs)


    def dispatch(self, request , *args, **kwargs) :
        if request.user.is_authenticated:
            return redirect('home:home')
        return super().dispatch(request,*args ,**kwargs)
    

    def get(self , request):
        form = self.form_class()
        return render(request , self.template_name , {'form':form})
    
    def post(self , request):
        form=self.form_class(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(request , phone_number =data['phone_number'] ,password = data['password'])
            if user is not None:
                login(request , user)
                messages.success(request , 'login successfully','success')
                if self.next:
                    return redirect(self.next)
                return redirect('home:home')
            messages.error(request ,'password or username is wrong' ,'warning')
        return render(request , self.template_name ,{'form':form})



class LogoutView( LoginRequiredMixin,View):
    def get(self , request):
        logout(request)
        messages.success(request ,'logout successfully','success')
        return redirect('home:home')
